package br.com.lsilva.threads.service;

import br.com.lsilva.threads.domain.*;
import br.com.lsilva.threads.model.RegistroThread;
import br.com.lsilva.threads.repository.RegistroRepository;

import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class RegistroThreadService {

    private static final Logger log = LoggerFactory.getLogger(RegistroThreadService.class);

    @Autowired
    private RegistroRepository registroRepository;

    public RegistroThread save(RegistroThread registroThread) {
        return this.registroRepository.save(registroThread);
    }

    @Async("truckMeTaskExecutor")
    public CompletableFuture<List<RegistroThread>> getHelloRepomTeste() throws InterruptedException {
        List<RegistroThread> registros = this.registroRepository.findAllLimit10();
        registros.forEach(registro -> {
            registro.setDataRegistro(new Date());
            registro.setStatus(Status.EM_PROCESSAMENTO);
            this.registroRepository.save(registro);
        });
        log.info("Executando Thread!!!!");
        
        Thread.sleep(5000L);

        registros.forEach(registro -> {
            registro.setDataRegistro(new Date());
            registro.setStatus(Status.PROCESSADO);
            this.registroRepository.save(registro);
        });
        return CompletableFuture.completedFuture(registros);
    }

    @Async("truckMeTaskExecutor")
    public CompletableFuture<List<RegistroThread>> getHelloRepomTeste2() throws InterruptedException {
        log.info("Executando Thread 2!!!!");
        Thread.sleep(1000L);
        List<RegistroThread> registros = this.registroRepository.findAllLimit10();
        registros.forEach(registro -> {
            registro.setDataRegistro(new Date());
            registro.setStatus(Status.PENDENTE);
            this.registroRepository.save(registro);
        });
        return CompletableFuture.completedFuture(registros);
    }
}
