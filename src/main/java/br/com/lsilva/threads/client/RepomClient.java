package br.com.lsilva.threads.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * RepomClient
 */
@Component
@FeignClient(value = "api.repom.teste", url = "http://localhost:8080/threads-2/v1")
    //url = "https://api-repom-teste.herokuapp.com/threads-2/v1")
public interface RepomClient {

    @GetMapping("/repom")
    String getHelloRepomTeste();
}