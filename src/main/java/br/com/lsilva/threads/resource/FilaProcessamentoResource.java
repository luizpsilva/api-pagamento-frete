package br.com.lsilva.threads.resource;

import br.com.lsilva.threads.client.RepomClient;
import br.com.lsilva.threads.model.RegistroThread;
import br.com.lsilva.threads.service.RegistroThreadService;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/processamento")
public class FilaProcessamentoResource {

    @Autowired
    private RepomClient repomClient;

    @Autowired
    private RegistroThreadService registroThreadService;

    @GetMapping("/thread")
    public ResponseEntity<?> getHelloRepomTeste() throws InterruptedException, ExecutionException {
        CompletableFuture<List<RegistroThread>> response = this.registroThreadService.getHelloRepomTeste();
        if(!response.get().isEmpty()) {
            return ResponseEntity.ok(response);
        }
        return ResponseEntity.badRequest().build();
    }

    @PostMapping("/save")
    public ResponseEntity<?> adicionarFilaProcessamento(@RequestBody RegistroThread registroThread) {
        RegistroThread registro = this.registroThreadService.save(registroThread);
        if(registro != null) {
            return ResponseEntity.ok(registro);
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/thread2")
    public ResponseEntity<?> getHelloRepomTeste2() throws InterruptedException, ExecutionException {
        CompletableFuture<List<RegistroThread>> response = this.registroThreadService.getHelloRepomTeste2();
        if(!response.get().isEmpty()) {
            return ResponseEntity.ok(response);
        }
        return ResponseEntity.badRequest().build();
    }
}
