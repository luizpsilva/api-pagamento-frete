package br.com.lsilva.threads;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ProjetoThreadsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetoThreadsApplication.class, args);
	}

}
