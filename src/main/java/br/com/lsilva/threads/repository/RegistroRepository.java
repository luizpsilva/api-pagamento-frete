package br.com.lsilva.threads.repository;

import br.com.lsilva.threads.model.RegistroThread;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RegistroRepository extends PagingAndSortingRepository<RegistroThread, Long> {

    @Query(nativeQuery = true, value="SELECT * FROM registro_thread ORDER BY id ASC LIMIT 10")
    List<RegistroThread> findAllLimit10();
}
