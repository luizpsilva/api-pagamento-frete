package br.com.lsilva.threads.domain;

public enum Status {

    PENDENTE("PENDENTE"),
    EM_PROCESSAMENTO("EM PROCESSAMENTO"),
    PROCESSADO("PROCESSADO");

    private String valor;

    Status(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }
}
