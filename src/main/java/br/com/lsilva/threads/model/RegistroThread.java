package br.com.lsilva.threads.model;

import br.com.lsilva.threads.domain.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class RegistroThread implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "data_registro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataRegistro;

    @Enumerated(EnumType.ORDINAL)
    private Status status;
}
